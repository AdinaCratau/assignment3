const FIRST_NAME = "Adina-Camelia";
const LAST_NAME = "Cratau";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
class Employee {
    constructor(name, surname, salary) {
        this.name = name;
        this.surname = surname;
        this.salary = salary;
        this.getDetails = function() {
            return name + ' ' + surname + ' ' + salary;
        };
    }
}

class SoftwareEngineer extends Employee {
    constructor(name, surname, salary, experience='JUNIOR') {
        super(name, surname, salary);
        this.experience = experience;
        this.applyBonus = function() {
            switch(this.experience) {
                case 'MIDDLE': {
                    salary = 1.15 * salary;
                    break;
                }
                case 'SENIOR': {
                    salary = 1.2 * salary;
                    break;
                }
                default: {
                    salary = 1.1 * salary;
                }
            }

            return salary;
        };
    }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}
